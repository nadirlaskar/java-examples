public class MultilevelInheritance{
	
	public static void main(String[] args){
		CParent parent = new CParent();
		CChildA childA = new CChildA();
		CChildB childB = new CChildB();
	}

}


class CParent{

	CParent(){
	  System.out.println("I am Parent Class.");
	}
}

class CChildA extends CParent{
	CChildA(){
	 System.out.println("I am ChildA class");
	}
}

class CChildB extends CChildA{
	CChildB(){
	 System.out.println("I am ChildB class");
	}
}