public class MultipleInheritance{
	public static void main(String[] args){
		//EChild child = new EChild();
	}
}

class EParent{
	EParent(){
	  System.out.println("I am Parent.");	
	}
}

class EParentA{
	EParentA(){
	  System.out.println("I am Child A.");	
	}
}

class EParentB{
	EParentB(){
	  System.out.println("I am Child B.");	
	}
}

//Multiple Inheritance not possible we use interface in java for this.
//class EChild extends EParent, EParentA,EParentB{
//	EChild(){
//	  System.out.println("I am Child C.");	
//	}
//}