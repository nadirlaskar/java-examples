public class HybridInheritance{
	public static void main(String[] args){
		HybridD hybrid = new HybridD();
		hybrid.MethodA();
		hybrid.MethodB();
		hybrid.MethodC();
	}
}


interface HybridA{

	public void MethodA();
}

interface HybridB extends HybridA{

	public void MethodB();
}

interface HybridC extends HybridA{

	public void MethodB();
}

class HybridD implements HybridB,HybridC{
	public void MethodA(){
		System.out.println("Implements Method A");
	}
	public void MethodB(){
		System.out.println("Implements Method B");
	}
	public void MethodC(){
		System.out.println("Implements Method C");
	}
}

