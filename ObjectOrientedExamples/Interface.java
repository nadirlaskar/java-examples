public class Interface {
	public static void main(String[] args){
		InterfaceImplementer interfaceExample = new InterfaceImplementer();
		interfaceExample.MethodA();
		interfaceExample.MethodB();
		interfaceExample.MethodC();
	}
}

interface InterfaceA{
	public void MethodA();
}

interface InterfaceB{
	public void MethodB();
}

interface InterfaceC{
	public void MethodC();
}

class InterfaceImplementer implements InterfaceA,InterfaceB,InterfaceC{
		public void MethodA(){
			System.out.println("From interface A");
		}
		
		public void MethodB(){
			System.out.println("From interface B");
		}
		
		public void MethodC(){
			System.out.println("From interface C");
		}
}



