//It is a compile time Polymorphism.

class Overloading{

	public static void main(String[] args){
		OverloadedArea area = new OverloadedArea();
		area.area(2);
		area.area(2,5);
	}
}

class OverloadedArea {
	void area(int x){
	  System.out.println("Value of x is "+x);
	}
	
	void area(int x,int y){
	  System.out.println("Value of x & y is "+x+" "+y);
	}
}