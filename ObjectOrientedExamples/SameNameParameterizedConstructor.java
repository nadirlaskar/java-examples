public class SameNameParameterizedConstructor
{
	public static void main(String[] args){
		SameNameParameterizedConstructorRoom myroom = new SameNameParameterizedConstructorRoom(20,40);
		myroom.displayArea();
	}

}

class SameNameParameterizedConstructorRoom{

	int length;
	int width;
	
	SameNameParameterizedConstructorRoom(int length,int width){
		this.length=length;
		this.width=width;
	}
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}