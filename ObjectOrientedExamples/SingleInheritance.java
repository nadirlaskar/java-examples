public class SingleInheritance{
	public static void main(String[] args){
		AChild child = new AChild();
		child.greet();
		child.getValue();
	}
}

class AParent{
	int value=50;
	void greet(){
	System.out.println("Hi ! from Parent.");
	}
		
}

class AChild extends AParent{

	int value=60;

	void greet(){
	System.out.println("Hi ! from Child.");
	}

	void getValue(){
	System.out.println(super.value);
	}
}