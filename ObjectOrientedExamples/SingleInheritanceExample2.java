public class SingleInheritanceExample2{
	public static void main(String[] args){
		BChild child = new BChild();
		BChild newchild = new BChild(15);
	}
}

class BParent{
	int value=50;
	BParent(){
	System.out.println("Hi ! from Parent.");
	}

	BParent(int value){
	System.out.println("Value Recieved: "+value+" from Parameter Constructor.");
	}
		
}

class BChild extends BParent{

	int value=60;

	BChild(){
	super();
	System.out.println("Hi ! from Child.");
	}

	BChild(int value){
	System.out.println("Value Recieved: "+value+" from Parameter Constructor.");
	}
}