public class Non_AccessSpecifier{
	//int x=10;    		//NonStatic variable cannot be accessed from static context. 
	static int x=10;
	
	public static void main(String[] args){

		final int a=20;

		System.out.println(x);
		//a+=30  //final variable can be initialized only once.
		System.out.println(a);
	}
} 