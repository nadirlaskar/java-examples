public class TwoConstructor
{
	public static void main(String[] args){
		TwoConstructorRoom mynewroom = new TwoConstructorRoom();
		mynewroom.displayArea();
		TwoConstructorRoom myroom = new TwoConstructorRoom(20,40);
		myroom.displayArea();
	}

}

class TwoConstructorRoom{

	int length;
	int width;
	
	TwoConstructorRoom(){
		length=30;
		width=60;
	}
	
	TwoConstructorRoom(int length,int width){
		this.length=length;
		this.width=width;
	}
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}