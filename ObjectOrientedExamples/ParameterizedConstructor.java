public class ParameterizedConstructor
{
	public static void main(String[] args){
		ParameterizedConstructorRoom myroom = new ParameterizedConstructorRoom(20,40);
		myroom.displayArea();
	}

}

class ParameterizedConstructorRoom{

	int length;
	int width;
	
	ParameterizedConstructorRoom(int l,int w){
		length=l;
		width=w;
	}
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}