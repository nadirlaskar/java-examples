public class AccessSpecifier{

	public static void main(String[] args){
	
	}
}

class AccessSpecifierA
{
	int value1=20;
	private int value2 = 40;

	void display(){
		System.out.println(value1);
	}
}

class AccessSpecifierB extends AccessSpecifierA
{
	value2=value2*value1;

	void area(){
		System.out.println(value1);
	}
}