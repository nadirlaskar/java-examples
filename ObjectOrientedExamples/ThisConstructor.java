public class ThisConstructor
{
	public static void main(String[] args){
		ThisConstructorRoom mynewroom = new ThisConstructorRoom();
		mynewroom.displayArea();
		ThisConstructorRoom myroom = new ThisConstructorRoom(20,40);
		myroom.displayArea();
	}

}

class ThisConstructorRoom{

	int length;
	int width;
	
	ThisConstructorRoom(){
		//this(1,1);
		length=30;
		width=60;
	}
	
	ThisConstructorRoom(int length,int width){
		this();
		this.length=length;
		this.width=width;
	}
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}