public class HierarchicalInheritance{
	public static void main(String[] args){
		DParent parent = new DParent();
		DChildA childA = new DChildA();
		DChildB childB = new DChildB();
		DChildC childC = new DChildC();
	}
}

class DParent{
	DParent(){
	  System.out.println("I am Parent.");	
	}
}

class DChildA extends DParent{
	DChildA(){
	  System.out.println("I am Child A.");	
	}
}

class DChildB extends DParent{
	DChildB(){
	  System.out.println("I am Child B.");	
	}
}

class DChildC extends DParent{
	DChildC(){
	  System.out.println("I am Child C.");	
	}
}