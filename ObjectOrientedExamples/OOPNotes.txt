Object Oriented.
Pure Object Oriented.
100% pure Object Oriented. 

Object Oriented Pillars:

1. Encapsulation.
2. Inheritance.
3. Polymorphism.

4. Everthing is written in class.
5. Everything is in terms of Class & Object.
6. Everything is in terms of method.


Language having 1,2,3 Pillar is a called Object Oriented.
 * C++
Language having 1,2,3,4 Pillar is a called Pure Object Oriented.
 * Java
Language having 1,2,3,4,5,6 Pillar is a called 100% Object Oriented.
 * Ruby, Full Stack


Encapsulation:
Hiding the data.

Inheritance:
Reuseability.

Super -> Sub.
Parent -> Child.

Polymorphism:
*Runtime Polymorphism.
  *Overloading.
*Compiletime Polymorphism.
  *Overriding.

In java if we have no constructor in java, JVM by default creates one.
If you create of any contructor JVM will not create one.

"this" keyword is used to refer to the current object instance.
"this" keyword is used to call the constructor of the current objects class.

::Constructor Vs Methods::

The constructor name should be same as the class name.
Constructor doesnot have return type, Method must have return type.
Constructor can be called only once, Method can be called many times.


Inheritance:

* Single Inheritance
* Multiple Inheritance
* Multilevel Inheritance
* Hybrid Inheritance
* Hierarchical Inheritance

Super is a keyword to call the variable, methods and constructors of the parent class.

Every subclass constructor calls its super constructor.
If we call super() then it doesn't call default super constructor.
The super keyword should be the first statement in constructor or method.

Interface defines the rues and egulation of a class.
Interface contains abstract methods.
Interface contains variables of constant type.

We cannot create the object of interface, We can only implement.

class extends class
interface extends interface 
class implements interface
 

Access Specifier:
Default:
Public:
Private:
Protected:

Non-Access Speccifier:
	     variable	method	  class
Static         yes	 yes	    no
Abstract        no	 yes	    yes	
Final	       yes	 yes	    yes	

8884316060
Pradeep@SunSoftTechies.com