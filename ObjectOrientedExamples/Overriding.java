//Overriding is Having Method with Same name and signature in superclass and subclass.

public class Overriding{

	public static void main(String[] args){
		Child child = new Child();
		child.display();

		Child newchild = (Child) new Parent(); 
		newchild.parentDisplay();
	}
}

class Parent{
  public void parentDisplay(){
	System.out.println("Hello! From Parent Class.");
  }
}

class Child extends Parent{
 public void display(){
	System.out.println("Hello! From Child Class.");
  }

 public void parentDisplay(){
	System.out.println("Hello! From Overrided Parent Method.");
  }
}