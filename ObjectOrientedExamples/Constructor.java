public class Constructor
{
	public static void main(String[] args){
		Room myroom = new Room();
		myroom.displayArea();
	}

}

class Room{

	int length;
	int width;
	
	Room(){
	  length=10;
	  width=20;
	}
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}