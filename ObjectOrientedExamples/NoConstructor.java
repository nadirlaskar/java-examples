public class NoConstructor
{
	public static void main(String[] args){
		NoConstructorRoom myroom = new NoConstructorRoom();
		myroom.length=10;
		myroom.width=20;
		myroom.displayArea();
	}

}

class NoConstructorRoom{

	int length;
	int width;
	
	public void displayArea(){
	  System.out.println("Area: "+(length*width));
	}
	
}