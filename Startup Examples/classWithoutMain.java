
class newclass{
	public void display(){
	System.out.println("This is from New Class");
}
}

public class classWithoutMain{

	//Static block get executed before main.

	static{	
		System.out.println("This is excecuted before main.");
		System.exit(0);	 // Main can never be reached as the code exits here.
	       }
	
	// Main never gets called.
	public static void main(String[] args){
		newclass n = new newclass();
		n.display();
	}

	// On JDK 6 and Below this code would have worked without main method. Since, the main was never reached.
	// From JDK 7 onwards, It was made mandatory to have main method to execute a ".class" file.
	// Even if main can never be reached.

}