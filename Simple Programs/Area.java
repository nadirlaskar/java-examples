public class Area
{
	public static void main(String[] args){
		Shape myshape= new Shape();

		myshape.Rectangle(10,20);
		myshape.Square(10);
		myshape.Triangle(10,20);
		myshape.Triangle(3,4,5);

	}

}

class Shape{

	
	void Rectangle(int length, int width){
		System.out.println("Area of Rectangle:"+(length*width));
	}
	
	
	void Square(int side){
		System.out.println("Area of Square: "+(side*side));
	}
	
	void Triangle(int base,int height){
		System.out.println("Area of RightAngleTriangle: "+((base*height)*0.5));
	}
	
	void Triangle(int side1,int side2, int side3){
		double side = ((side1+side2+side3)*0.5);
		System.out.println("Area of Triangle: "+((side-side1)*(side-side2)*(side-side3)));
	}
	
}