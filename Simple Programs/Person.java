public class Person
{
	public static void main(String[] args){
		PersonData person = new PersonData("Nadir",23,20000);
		person.displayName();
		person.displayAge();
		person.displaySalary();

	}

}

class PersonData{

	private String Name;
	private int Age;
	private double Salary;	

	PersonData(String Name,int Age, double Salary){
		this.Name=Name;
		this.Age=Age;
		this.Salary=Salary;
	}
	
	
	String displayName(){
		System.out.println("Name: "+Name);
		return Name;
	}
	
	int displayAge(){
		System.out.println("Age: "+Age);
		return Age;
	}
		
	double displaySalary(){
		System.out.println("Salary: $"+Salary);
		return Salary;
	}
	
}