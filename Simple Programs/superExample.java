public class superExample{
	
	public static void main(String[] args){
		ChildB childB = new ChildB();
		
	}
}

class Root{

	Root(){
	  System.out.println("I am Root !");
	}

	public void Greet(){
	  System.out.println("Hello World ! - Root");
	}	
}

class ChildA extends Root{
	
	ChildA(){
	  System.out.println("I am ChildA !");	
	}

	public void Greet(){
	  System.out.println("Hello World ! - ChildA");
	}
	
}

class ChildB extends ChildA{
	
	ChildB(){
	  System.out.println("I am ChildB !");	
	}

	public void Greet(){
	  System.out.println("Hello World ! - ChildB");
	}

}