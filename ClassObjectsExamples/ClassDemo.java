public class ClassDemo{

	public static void main(String[] args){
	//A a = new A();   // Object cannot be created, A is a sibling class of type private.
	//B b = new B();   // Object cannot be created, B is a sibling  class of type public.

	C c = new C();     // creating object of static inner class from main method.
	D d = new D();	   // Creating object of default type sibling class.	

	P.Q q = new P().new Q(); // Creating Object & Storing reference of Inner Class Q of P.

	//a.display();     // Object not created, Cannot call the method.
	//b.display();	   // Object not created, Cannot call the method.

	c.display();   
	d.display();

 	q.display(); 
	
        new P().new Q().display(); // Creating Inner class Object and calling Method, Without storing ref.
 
	P p_ = new P();		// Creating Object Ref of P	
	P.Q q_ = p_.new Q(); 	// Creating Object Ref of Q using object reference p_.
	q_.display();		// Calling display method using object reference q_.

	q_.displayR();          // Calling display method of private inner class R of P using displayR() method of Q.
	p_.displayR(); 		// Calling display method of private inner class R of P using displayR() method of P.

	}
	

	// To create object of a inner class from a static method,
	//class should be declared static.

	public static class C{

		void display(){
		  System.out.println("From C");
		}
	
	}

}

class D{
   void display(){
	System.out.println("From D");
    }

}

class P{

       public class Q{
		void display(){
		System.out.println("From Q");
		}
		
		void displayR(){
		System.out.println("Calling using method of Q: ");
		new R().display();
		}
	}
	
	void displayR(){
		System.out.println("Calling using method of P: ");
		new R().display();
		}

	// Can be accessed only by the methods of the outer class (i.e P) and inner classes (i.e Q).
	private class R{
		void display(){
		System.out.println("From R"); 
		}
	}
}


//private classes are allowed only as a inner class.
//private class A{
//	void display(){
//		System.out.println("From A");
//	}
//}

//This class will cause error at compile time,
//Only one public class can exist in a single .java file.
//Public classes should have the class name same as the filename.

//public class B{
//	void display(){
//		System.out.println("From B");
//	}
//}