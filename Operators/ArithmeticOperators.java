public class ArithmeticOperators{


	public static void main(String[] args){
		ArithmeticOperators Operators = new ArithmeticOperators();
		Operators.add(5,6);
		Operators.subs(6,5);
		Operators.mul(6,5);
		Operators.div(5,6);
		Operators.mod(68,24);
	}

	
	private void add(int a, int b){
	
		System.out.println("Result of "+ a + " + " + b + " is "+(a+b));
	}

	private void subs(int a, int b){
	
		System.out.println("Result of "+ a + " - " + b + " is "+(a-b));
	}

	private void mul(int a, int b){
	
		System.out.println("Result of "+ a + " * " + b + " is "+(a*b));
	}
	
	private void div(int a, int b){
		if(a==0||b==0) {System.out.println("Result of "+ a + " / " + b + " is 0"); return;}
		System.out.println("Result of "+ a + " / " + b + " is "+(float)(a/b));
	}
	
	private void mod(int a, int b){
		if(a==0||b==0) {System.out.println("Result of "+ a + " % " + b + " is 0"); return;}
		System.out.println("Result of "+ a + " % " + b + " is "+(a%b));
	}
	
}