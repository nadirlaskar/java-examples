public class IncrementOperators{

	public static void main(String[] args){
	
		IncrementOperators operator= new IncrementOperators();
		
		int a = 10;
		operator.preIncrementAdd(a);
		operator.postIncrementAdd(a);	

		operator.preIncrementSub(a);
		operator.postIncrementSub(a);
		
		operator.someExamples();
		
		
		
		
	}

	private void preIncrementAdd(int a){
	
		System.out.println("++a("+a+") is : "+(++a));

	}
	
	private void preIncrementSub(int a){
	
		System.out.println("--a("+a+") is : "+(--a));

	}

	private void postIncrementAdd(int a){
	
		System.out.println("a++("+a+") is : "+(a++));

	}
	
	private void postIncrementSub(int a){
	
		System.out.println("a--("+a+") is : "+(a--));

	}

	private void someExamples(){
		
		System.out.println("\n\n********* Some Examples ******\n\n");

		int p=10,q=10;
		System.out.println("p : "+p+" q : "+q);
		p++;
		q++;
		System.out.println("p++ : "+p);
		System.out.println("++q : "+q);

		int r=20;
		System.out.println("\nr : "+r);
		System.out.println("r++ "+(r++));
		r+=2;
		System.out.println("r+=2"+(r+=2));

		r=20;
		System.out.println("\nr : "+r);
		System.out.println("++r "+(++r));
		r+=2;
		System.out.println("r+=2"+(r+=2));

		int v=400;
		int u =398;
		System.out.println("\nv : "+v+" u : "+u);
		System.out.println("v+++1 "+(v+++1));
		v=400;u=398;
		System.out.println("\nv : "+v+" u : "+u);
		System.out.println("--u+++u "+(--v+u));
		
		//int n,m,o;
		//n=200;m=300;o=400;
		//System.out.println("--a++=b-c--"+(--n+++m-o--)); //The statement will throw error.

	}
	

}