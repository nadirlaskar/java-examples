public class BinaryOperators{

	public static void main(String[] args){
	
		BinaryOperators operator= new BinaryOperators();
		boolean a = true;
		boolean b = false;
		int c=20,d=20;
		
		operator.BinaryAND(a,b);
		operator.BinaryOR(a,b);	

		operator.BinaryAND(c,d);
		operator.BinaryOR(c,d);	

		operator.BinaryLeftShift(20,1);
		operator.BinaryRightShift(20,2);

		
		
	}

	private void BinaryAND(boolean a, boolean b){
	
		System.out.println(a+" & "+b+" is : "+(a&b));

	}
	
	private void BinaryOR(boolean a, boolean b){
	
		System.out.println(a+" | "+b+" is : "+(a|b));

	}

	private void BinaryAND(int a, int b){
	
		System.out.println(a+" & "+b+" is : "+(a&b));

	}
	
	private void BinaryOR(int a, int b){
	
		System.out.println(a+" | "+b+" is : "+(a|b));

	}

	private void BinaryLeftShift(int a, int b){
	
		System.out.println(a+" << "+b+" is : "+(a<<b));

	}

	private void BinaryRightShift(int a, int b){
	
		System.out.println(a+" >> "+b+" is : "+(a>>b));

	}

}