public class LogicalOperators{

	public static void main(String[] args){
	
		LogicalOperators operator= new LogicalOperators();
		boolean a = true;
		boolean b = false;
		int c=20,d=20;
		
		operator.logicalAND(a,b);
		operator.logicalOR(a,b);	
		operator.logicalGreaterThan(c,d);
		operator.logicalLessThan(c,d);
		operator.logicalGreaterThanEqual(c,d);
		operator.logicalLessThanEqual(c,d);
		
	}

	private void logicalAND(boolean a, boolean b){
	
		System.out.println(a+" && "+b+" is : "+(a&&b));

	}
	
	private void logicalOR(boolean a, boolean b){
	
		System.out.println(a+" || "+b+" is : "+(a||b));

	}

	private void logicalGreaterThan(int a, int b){
	
		System.out.println(a+" > "+b+" is : "+(a>b));

	}

	private void logicalLessThan(int a, int b){
	
		System.out.println(a+" < "+b+" is : "+(a<b));

	}

	private void logicalLessThanEqual(int a, int b){
	
		System.out.println(a+" <= "+b+" is : "+(a<=b));

	}
	
	private void logicalGreaterThanEqual(int a, int b){
	
		System.out.println(a+" >= "+b+" is : "+(a>=b));

	}
}